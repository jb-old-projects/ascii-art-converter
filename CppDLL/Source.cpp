/**********************************************/
/* Autor: Jakub Biskup
/* Temat: Konwersja binarnego ascii art
/* Przedmiot: J�zyki Asemblerowe
/* Prowadz�cy: mgr. in�. Jaros�aw Paduch
/* Data ostatniej modyfikacji: 2018-01-31
/*
/* Opis: Plik zawieraj�cy bibliotek� cpp
/*
/* CHANGELOG:
/* v0.5:
/* - przeczyszczenie kodu
/*
/* v0.4:
/* - dodanie nag��wka
/*
/* v0.3:
/* - uporz�dkowano cia�o metody
/*
/* v0.2:
/* - stworzono cia�o metody
/*
/* v0.1:
/* - stworzono metode bez cia�a
/**********************************************/


#include <iostream>

//cpp dll counts instances of ones and zeros
//returns 0 if instances of zeros > onese
//else it returns 1
extern "C" __declspec(dllexport) int ProcessCpp(unsigned char _pictureFragment[], int tabStartIndex, int size)
{
	int zerosCounter = 0;
	int onesCounter = 0;

	for (int i = tabStartIndex; i < size; i++)
	{
		if (_pictureFragment[i] == '0')
		{
			zerosCounter++;
		}

		if (_pictureFragment[i] == '1')
		{
			onesCounter++;
		}
	}

	return zerosCounter > onesCounter ? 0 : 1;
}