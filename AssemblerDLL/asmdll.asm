;;---------------------------------------------
;; Autor: Jakub Biskup
;; Temat: Konwersja binarnego ascii art
;; Przedmiot: J�zyki Asemblerowe
;; Prowadz�cy: mgr. in�. Jaros�aw Paduch
;; Data ostatniej modyfikacji: 2018-01-31
;;
;;Opis: Plik zawieraj�cy metode w wersji asemblerowej
;;
;; CHANGELOG:
;; v0.4:
;; - dodanie mietody w kt�rej wyst�puje za�lepka z wykorzystaniem rozkaz�w wektorowych
;; - dodanie komentarzy i opis�w
;;
;; v0.3:
;; - dodanie nag��wka
;;
;; v0.2:
;; - dodanie tymczasowego cia�a funkcji
;;
;; v0.1:
;; - stworzono plik
;;---------------------------------------------


.DATA
onesCounter byte 0		; zmienna s�u��ca jako licznik jedynek
zerosCounter byte 0		; zmienna s�u��ca jako licznik zer
.CODE

;--------------------------------------------------------------------------------------------------
;						Opis procedur ProcessAsm oraz ProcessAsmAVX
; 
; 
; ---  ProcessAsm ---
;
; Procedura zliczaj�ca wyst�pnienia zer i jedynek w tablicy bajtw�, procedura zwraca
; jedynk� je�eli jedynek jest wi�cej lub tyle samo co zer, w przeciwnym wypadku zwraca zero.
; Procedura przyjmuje 3 parametry. Pierwszy [rcx] - tablica bajt�w, drugi [rdx] - pocz�tek tablicy,
; trzeci [r8] - rozmiar tablicy.
;
; Przez instrukcje warunkowe oraz zmienne zale�ne od tych instrukcji nie by�em w stanie znale��
; praktycznego zastosowania rozkaz�w wektorowych, dlatego zosta�a wstawiona za�lepka kt�rej 
; funkcjonalno�� to operacja OR na dw�ch takich samych warto�ciach
; 
;--------------------------------------------------------------------------------------------------


ProcessAsm proc tab: byte, tabStart: dword, tabSize: dword
	
	mov ah, 00110001b			; liczba 49 binarnie, odpowiednik 1 w ascii, zapisana w 8-bit rejestrze ah, s�u�y do por�wnania z elementami tablicy
	mov rbx, rdx				; przypisanie zawarto�ci rdx do rbx w celu wykorzystania przy ponownym przechodzeniu po tablicy

mainLoop:
	mov bh, [rcx+rdx]			; przypisanie elementu tablicy (rcx - pocz�tek tablicy, rdx - offset) do rejestru bh
	cmp ah, bh					; por�wnanie zawarto�ci rejestru bh do ah - czy jest jedynk�
	je incOnesCounter			; je�eli bh == ah to skok do incOnesCounter (bh jest jedynk�)
	jne incZerosCounter			; je�eli bh =/= ah to skok do incZerosCounter (bh jest zerem)
contLoop:
	inc rdx						; inkrementacja rdx (offsetu)
	cmp rdx, r8					; sprawdzenie przekroczenia tablicy
	je checkAvxRange			; je�eli s� r�wne to skok do checkAvxRange
	jmp mainLoop				; je�eli nie s� r�wne to przeskakujemy do mianLoop i wykonujemy operacje na nast�pnym elemencie tablicy
		
incOnesCounter:
	inc onesCounter				; inkrementacja zawarato�ci onesCounter
	jmp contLoop				; bezwarunkowy skok do contLoop

incZerosCounter:
	inc zerosCounter			; inkrementacja zawarato�ci zerosCounter
	jmp contLoop				; bezwaruknowy skok do contLoop

checkAvxRange:
	add rbx, 10h				; dodanie 16 (rozmiar rejestru xmm) do rbx
	cmp r8, rbx					; sprawdzenie przekroczenia tablicy
	js procEnd					; je�li adres przekroczy zakres edycji tablicy to przeskakujemy do procEnd
	sub rbx, 10h				; przywr�cenie pocz�tkowej warto�ci rbx 
	jmp avxLoop					; skok do p�tli programu, kt�ra wykonuje operacje z wykorzystaniem rozkaz�w wektorowych

avxLoop:
	vmovups xmm2, [rcx+rbx]		; rozkaz vmovups s�u�y do przenoszenia danych z argumentu drugiego do argumentu pierwszego
								; w tym przypadku zostan� przeniesione 16 bajty danych z naszej tablicy do rejestru ymm2
	vorps xmm2, xmm2, xmm2		; wektorowy rozkaz kt�ry do pierwszego argumentu zwraca "or" argument�w 2 i 3
	vmovups [rcx+rbx], xmm2		; przeniesienie zanegowanych danych z rejestru ymm0 na ich miejsce w naszej tablicy
	add rbx, 10h				; dodanie 16 (rozmiar rejestru xmm) do rbx
	cmp rbx, r8					; sprawdzenie przekroczenia tablicy
	je procEnd					; je�li adres przekroczy zakres edycji tablicy to przeskakujemy do procEnd
	jmp avxLoop					; je�eli adres nie przekracza to bezwarunkowy skok do avxLoop

procEnd:
	mov al, onesCounter			; przypisanie zawarto�ci onesCounter do rejestru al
	mov bl, zerosCounter		; przypisanie zawarto�ci zerosCounter do rejestru bl
	mov onesCounter, 0			; przypisanie zera do onesCounter
	mov zerosCounter, 0			; przypisanie zera do zerosCounter
	cmp al, bl					; por�wnanie zawarto�ci bl do al (sprawdzanie czy jest wi�cej zer czy jedynek)
	jge retOne					; je�eli al >= bl skok do retOne (wi�cej lub tyle samo jedynek)
	jl retZero					; je�eli al < bl skok do retZero (wi�cej zer)

retOne:
	mov eax, 1
	ret							; wyj�cie z procedury 
retZero:
	mov eax, 0
	ret							; wyj�cie z procedury 

ProcessAsm endp					; zako�czenie procedury

END

