﻿/**********************************************/
/* Autor: Jakub Biskup
/* Temat: Konwersja binarnego ascii art
/* Przedmiot: Języki Asemblerowe
/* Prowadzący: mgr. inż. Jarosław Paduch
/* Data ostatniej modyfikacji: 2018-01-27
/* 
 * Opis: Jest to główna klasa aplikacji, w której znajduje się metoda Main
/**********************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projekt_JA
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
