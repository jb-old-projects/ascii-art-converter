﻿/**********************************************/
/* Autor: Jakub Biskup
/* Temat: Konwersja binarnego ascii art
/* Przedmiot: Języki Asemblerowe
/* Prowadzący: mgr. inż. Jarosław Paduch
/* Data ostatniej modyfikacji: 2018-01-31
/* 
/* CHANGELOG:
/* v0.5:
/* - dodanie wywołania dll w asm 
/* - przeczyszczenie kodu
/* - naprawa błedów
/*  
/* v0.4:
/* - wydzielono fragmenty kodu i utworzenie z nich metod
/* - dodanie nagłówka
/* 
/* v0.3:
/* - dodano algorytm rozbijający obraz na fragmenty
/* - dodano algorytm scalajacy skonwertowane fragmenty
/* - dodano zapis do pliku .txt
/* 
/* v0.2:
/* - upgrade interfejsu użytkownika
/* - usystematyzowano nazwy metod
/* - dodano cała logika zwiazana z UI
/* - wrzucono tymczasowe zmienne i nazwy
/* - przeczyszczono projekt
/* 
/* v0.1:
/* - stworzenie pliku
/**********************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projekt_JA
{
    public partial class Form1 : Form
    {
        private OpenFileDialog ofd = new OpenFileDialog();
        private SaveFileDialog sfd = new SaveFileDialog();

        private List<byte[]> sourceFileData = new List<byte[]>();
        private DLL dll = DLL.Cpp;
        private int accuracy = 2;
        private bool messageBoxAvailable = false;

        [DllImport("C:\\Users\\Jakub\\Downloads\\Projekt-JA\\Projekt-JA\\x64\\Release\\CppDLL.dll")]
        public static extern int ProcessCpp(byte[] pictureFragment, int tabStartIndex, int size);

        [DllImport("C:\\Users\\Jakub\\Downloads\\Projekt-JA\\Projekt-JA\\x64\\Release\\AssemblerDLL.dll")]
        public static extern int ProcessAsm(byte[] pictureFragment, int tabStartIndex, int size);


        public Form1()
        {
            InitializeComponent();
            SetThreadsNumber();
            SetDefaultDll();
            radioButton3.Checked = true;
        }

        //SetThreadNumber sets default threads number
        //which is equal to number of threads
        //in the computer
        private void SetThreadsNumber()
        {
            trackBar1.Value = Environment.ProcessorCount;
            label3.Text = trackBar1.Value.ToString();
        }


        //SetDefaultDll sets cpp ddl as default
        private void SetDefaultDll()
        {
            radioButton1.Checked = true;
            dll = DLL.Cpp; ;
        }

        //OnOpenFileButtonClick handles opening file 
        //and saves it's path to the variable
        private void OnOpenFileButtonClick(object sender, EventArgs e)
        {
            ofd.Filter = "TXT|*.txt";
            sourceFileData.Clear();

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = ofd.FileName;

            }
        }

        //LoadFileData loads data from file to sourceFileData
        private void LoadFileData()
        {
            sourceFileData.Clear();
            string[] sourceFileDataAsString = File.ReadAllLines(ofd.FileName);

            foreach (var line in sourceFileDataAsString)
            {
                sourceFileData.Add(Encoding.ASCII.GetBytes(line));
            }
        }


        //OnSaveFileButtonClick gets name of the file where result will be saved
        private void OnSaveFileButtonClick(object sender, EventArgs e)
        {
            sfd.Filter = "TXT|*.txt";
            sfd.Title = "Save an text File";

            sfd.ShowDialog();

            if (sfd.FileName != "")
            {
                textBox2.Text = sfd.FileName;
            }
        }

        //OnThreadsTrackbarValuChanged gets information
        //how many threads will be used
        private void OnThreadsTrackbarValuChanged(object sender, EventArgs e)
        {
            label3.Text = trackBar1.Value.ToString();
        }

        //OnCppDllRadioButtonClick sets dll to dll.cpp
        private void OnCppDllRadioButtonClick(object sender, EventArgs e)
        {
            dll = DLL.Cpp;
        }

        //OnCppDllRadioButtonClick sets dll to dll.asm
        private void OnAsmDllRadioButtonClick(object sender, EventArgs e)
        {
            dll = DLL.Asm;
        }


        //divideInSquares divides input file into squares
        List<byte[]> DivideInSquares()
        {
            List<byte[]> squares = new List<byte[]>();

            for (int i = 0; i <= sourceFileData.Count - accuracy; i += accuracy)
            {
                for (int j = 0; j <= sourceFileData[i].Length - accuracy; j += accuracy)
                {
                    byte[] tmpArr = new byte[accuracy * accuracy];
                    int tmpIndexI = i;
                    int tmpIndexJ = j;

                    for (int k = 0; k < accuracy * accuracy; k++, tmpIndexJ++)
                    {
                        if (k != 0 && k % accuracy == 0)
                        {
                            tmpIndexI++;
                            tmpIndexJ = j;
                        }

                        tmpArr[k] = sourceFileData[tmpIndexI][tmpIndexJ];
                    }

                    squares.Add(tmpArr);
                }
            }
            return squares;
        }

        //mergeSquares merges converted squares into one file
        void MergeSquares(List<byte[]> _source)
        {
            List<byte[]> squares = new List<byte[]>();

            for (int i = 0, k = 0; i <= sourceFileData.Count - accuracy; i += accuracy)
            {
                for (int j = 0; j <= sourceFileData[i].Length - accuracy; j += accuracy, k++)
                {
                    int tmpIndexI = i;
                    int tmpIndexJ = j;

                    for (int m = 0; m < accuracy * accuracy; m++, tmpIndexJ++)
                    {
                        if (m != 0 && m % accuracy == 0)
                        {
                            tmpIndexI++;
                            tmpIndexJ = j;
                        }
                        sourceFileData[tmpIndexI][tmpIndexJ] = _source[k][m];
                    }
                }
            }
        }

        //OnRunButtonClicked starts conversion process, but first
        //it checks if everything is ok
        private void OnRunButtonClicked(object sender, EventArgs e)
        {
            if (!AllFieldsFilled())
            {
                return;
            }

            if (!IsSquare())
            {
                return;
            }

            LoadFileData();

            if (!IsProperData())
            {
                return;
            }

            if (!IsProperAccuracy())
            {
                return;
            }

            List<byte[]> dllData = new List<byte[]>(DivideInSquares());

            if (!IsProperThreadsCount(dllData.Count))
            {
                return;
            }

            List<Thread> currentThreads = new List<Thread>();
            List<int> results = new List<int>();
            int squaresNoEditedCount = dllData.Count;

            int currentIndex = 0;

            for (int i = 0; i < trackBar1.Value; i++)
            {
                int threadsLeft = trackBar1.Value - i;
                int toAdd = squaresNoEditedCount % threadsLeft != 0 ? 1 : 0;
                int squaresToEditCount = squaresNoEditedCount / threadsLeft + toAdd;

                currentThreads.Add(new Thread(() =>
                {
                    int minIndex = currentIndex;
                    int maxIndex = currentIndex + squaresToEditCount;

                    while (squaresToEditCount > 0)
                    {
                        int index = dllData.Count - squaresNoEditedCount;

                        if (index == dllData.Count)
                        {
                            index--;
                        }

                        if (dll == DLL.Cpp)
                        {
                            results.Add(ProcessCpp(dllData[index], 0, dllData[index].Length));
                        }
                        else if (dll == DLL.Asm)
                        {
                            results.Add(ProcessAsm(dllData[index], 0, dllData[index].Length));
                        }

                        squaresToEditCount--;

                        if (squaresNoEditedCount > 0)
                        {
                            squaresNoEditedCount--;
                        }
                    }
                }));

                currentIndex += squaresToEditCount;
            }

            System.Diagnostics.Stopwatch timer = System.Diagnostics.Stopwatch.StartNew();

            foreach (var thread in currentThreads)
            {
                thread.Start();
            }

            foreach (var thread in currentThreads)
            {
                thread.Join();
            }

            timer.Stop();

            for (int i = 0; i < dllData.Count; i++)
            {
                dllData[i] = ChangeSquare(dllData[i], results[i]);
            }

            MergeSquares(dllData);

            ShowTimeResults(timer);

            SaveResultFile();
        }

        private void ShowTimeResults(System.Diagnostics.Stopwatch timer)
        {
            if (messageBoxAvailable)
            {
                MessageBox.Show("Time: " + (timer.ElapsedMilliseconds).ToString() + "ms", "Result");
                messageBoxAvailable = false;
            }
            else
            {
                messageBoxAvailable = !messageBoxAvailable;
            }
        }

        private bool IsProperThreadsCount(int squaresCount)
        {
            if (trackBar1.Value <= squaresCount)
            {
                return true;
            }
            else
            {
                if (messageBoxAvailable)
                {
                    MessageBox.Show("Invalid threads count! Maximum threads count for this data is: " + squaresCount);
                    messageBoxAvailable = false;
                }
                else
                {
                    messageBoxAvailable = true;
                }
                return false;
            }
        }

        //AllFieldsFilled checks if open and save fields are filed
        private bool AllFieldsFilled()
        {
            if (ofd.FileName == "" || sfd.FileName == "")
            {
                if (messageBoxAvailable)
                {
                    MessageBox.Show("You must fill all fields to run application!", "Warning");
                    messageBoxAvailable = false;
                }
                else
                {
                    messageBoxAvailable = true;
                }
                return false;
            }
            else
            {
                return true;
            }
        }

        //IsSquare checks if input file is square
        private bool IsSquare()
        {
            foreach (var line in sourceFileData)
            {
                if (line.Length != sourceFileData.Count)
                {
                    if (messageBoxAvailable)
                    {
                        MessageBox.Show("Invalid image!", "Warning");
                        messageBoxAvailable = false;
                    }
                    else
                    {
                        messageBoxAvailable = true;
                    }
                    return false;
                }
            }

            return true;
        }

        //IsProperAccuracy checks if converting in choosen accuracy is possible
        private bool IsProperAccuracy()
        {
            if (sourceFileData.Count % accuracy == 0)
            {
                return true;
            }
            else
            {
                List<int> availableAccuracies = GetAvailableAccuracies();

                string message = "You must choose all fields to run application!\n";

                if (availableAccuracies.Count == 0)
                {
                    message += "Invalid image! No available accuracy!";
                }
                else
                {
                    message += "Available accuracy: ";

                    foreach (var acc in availableAccuracies)
                    {
                        message += string.Format("{0}, ", acc.ToString());
                    }
                }

                if (messageBoxAvailable)
                {
                    MessageBox.Show(message, "Warning");
                    messageBoxAvailable = false;
                }
                else
                {
                    messageBoxAvailable = true;
                }
                return false;
            }
        }

        //IsProperData checks if in file are only ones and zeros
        private bool IsProperData()
        {
            byte zeroAsByte = Convert.ToByte('0');
            byte oneAsByte = Convert.ToByte('1');

            foreach (var line in sourceFileData)
            {
                foreach (var character in line)
                {
                    if (character == zeroAsByte || character == oneAsByte)
                    {
                        continue;
                    }
                    else
                    {
                        if (messageBoxAvailable)
                        {
                            MessageBox.Show("Invalid data (probably file contains not only 0 and 1 chars)!", "Warning");
                            messageBoxAvailable = false;
                        }
                        else
                        {
                            messageBoxAvailable = true;
                        }
                        return false;
                    }
                }
            }

            return true;
        }

        //GetAvailableAccuracies chcecks which accuracy is possible
        private List<int> GetAvailableAccuracies()
        {
            List<int> accuracies = new List<int>();

            if (sourceFileData.Count % 2 == 0)
            {
                accuracies.Add(2);
            }
            if (sourceFileData.Count % 4 == 0)
            {
                accuracies.Add(4);
            }
            if (sourceFileData.Count % 8 == 0)
            {
                accuracies.Add(8);
            }

            return accuracies;
        }

        //ChangeSquare converts squares base on the result of the dll
        private byte[] ChangeSquare(byte[] square, int resultValue)
        {
            char tmp;

            if (resultValue == 0)
            {
                tmp = '0';
            }
            else
            {
                tmp = '1';
            }

            for (int i = 0; i < square.Length; i++)
            {
                square[i] = Convert.ToByte(tmp);
            }

            return square;
        }

        //SaveResultFile saves result of the program into .txt file
        private void SaveResultFile()
        {
            int size = sourceFileData.Count * sourceFileData.Count;
            byte[] resultByteArr = new byte[size];

            for (int i = 0; i < size; i++)
            {
                resultByteArr[i] = sourceFileData[i % sourceFileData.Count][i / sourceFileData.Count];
            }

            string[] arr = new string[sourceFileData.Count];

            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = Encoding.ASCII.GetString(sourceFileData[i]);
            }

            File.WriteAllLines(sfd.FileName, arr);
        }

        //SetAccuracyToX handles accuracy radio buttons
        private void SetAccuracyToTwo(object sender, EventArgs e)
        {
            accuracy = 2;
        }

        private void SetAccuracyToFour(object sender, EventArgs e)
        {
            accuracy = 4;
        }

        private void SetAccuracyToEight(object sender, EventArgs e)
        {
            accuracy = 8;
        }
    }
}

public class MessageBoxWrapper
{
    public static bool IsOpen { get; set; }

    // give all arguments you want to have for your MSGBox
    public static void Show(string messageBoxText, string caption)
    {
        IsOpen = true;
        MessageBox.Show(messageBoxText, caption);
        IsOpen = false;
    }
}

public enum DLL
{
    Cpp,
    Asm,
}
